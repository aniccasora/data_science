import math
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.linear_model import Lasso
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split

if __name__== "__main__":
    cancer = load_breast_cancer()  #讀入資料
    # type(cancer)       # <class 'sklearn.utils.Bunch'>
    # type(cancer.keys() # <class 'dict_keys'>

    # DataFrame() 方法將一個 dictionary 的資料結構轉換成 data frame(Two-dimensional)
    # cancer.feature_names : 各特徵的名稱(字串)
    # columns = 給予 column label, 不給時則會 用 0,1,2,...,n
    cancer_df = pd.DataFrame(cancer.data, columns=cancer.feature_names)

    X = cancer.data # 各特徵的數值(只有數值，這資料及每筆資料都有數個 feature)
    Y = cancer.target # label     (1 or 0)

    # 資料分割成 train 和 test。
    # 當成隨機種子ㄅ，random_state 可以在拆分資料集時先洗牌。
    rst = np.random.RandomState()
    X_train, X_test, y_train, y_test = \
        train_test_split(X, Y, test_size=0.33, random_state=rst)

    # 將 lasso regression 依不同 alpha值 算完的相關係數 記錄下來。
    lasso_coef_list = []
    # for-loop 會把 list 的 alpha 都 fit 一遍。
    alpha_list = [0, 0.001, 0.0005, 0.00008]
    #------
    for alpha in alpha_list:
        # alpha 是用来調整 lasso的懲罰強度的 "超引數"。如果是0，lasso就會是線性回歸。
        lasso = Lasso(alpha=alpha, normalize=True)
        lasso.fit(X_train, y_train) # fit=訓練
        # 紀錄算好的 coef
        lasso_coef_list.append(lasso.coef_)
        # 預測的 R^2(決定係數) (訓練資料集)。
        train_score=lasso.score(X_train, y_train)
        # 預測的 R^2(決定係數) (測試資料集)。
        test_score=lasso.score(X_test, y_test)

        # lasso.coef_ != 0 : 左方是 ndarray。
        # 這個計算為 each-item opeartor。 他會回傳 size(lasso.coef_) 個 "boolean" 結果。
        coeff_used = np.sum(lasso.coef_ != 0) # 找非零係數
        print ("training R^2:", train_score)
        print ("test R^2: ", test_score)
        print ("number of features used: ", coeff_used)
        print("\n")

    # 開始畫畫
    for idx, alpha in enumerate(alpha_list):
        fig, ax = plt.subplots()
        plt.title("lasso a={:f}".format(alpha), {'fontsize' : 17})
        plt.ylabel("coefficents")# 設定y軸標題
        plt.xlabel("variable")# 設定x軸標題
        x = [i+1 for i in range(30)]
        y = lasso_coef_list[idx]

        # 存放 土炮合併的結果。
        tmp_list = []
        # 土炮合併, 因為我想透過 coef 去排序圖表，但也要顧到 label順序，不能 coef 排了, label 不排。
        for i in range(len(y)):
            tmp_list[len(tmp_list):] = [(y[i],cancer['feature_names'][i])]

        # 一筆 "土炮合併"後的 "單筆" 資料組成為 (R^2係數(int), 其標籤名稱("字串"))
        # 那個 第二筆 tuple 資料是string 時你要給他你允許的最大字串 ( S+"最大字數")
        # 於是我們定義 data type。
        dtype = [('coef', int), ('feature_name', 'S50')]
        # 土炮合併後的結果。
        merged = np.array(tmp_list, dtype=dtype)
        # print("土炮合併:", merged)
        # 這樣要排序時 就可以指定你要排序的 label 依據，這個 array 我用 'coef' 來排序。
        sorted_array = np.sort(merged,order='coef')
        # print("土炮合併(排序後):", sorted_array)
        #  大概像是-> [(-2, "AA") (-1, "BA") ... (5,"CC")]

        # 接下來我們依據 sorted_array 重新定 y 跟 label 的資料就行了。
        tmp_list = [] # for coef
        tmp_list_1 = [] # for label
        # 土炮分割
        for i in range(len(y)):
            tmp_list.append(sorted_array[i][0])
            tmp_list_1.append(sorted_array[i][1])
        sorted_y = np.array(tmp_list)
        sorted_label = np.array(tmp_list_1)

        ax.set_xticks(x) # 設定 X 刻度
        ax.set_xticklabels(sorted_label)
        fig.autofmt_xdate(rotation=90) # 不旋轉的話字全部寫橫的黏在一起就不用看了。

        plt.bar(x, sorted_y)
    plt.show()

