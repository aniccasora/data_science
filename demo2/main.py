from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import plot_confusion_matrix
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    cancer = datasets.load_breast_cancer()
    X = cancer.data
    y = cancer.target

    # 分割資料
    X_train, X_test, y_train, y_test = train_test_split(X, y,test_size = 0.2, random_state=5)

    # 設定 degree 順序。
    use_degree = [1,2,5]

    for degree in use_degree:
        print("DEGREE =",degree) # 印出現在的 title
        classifier = SVC(kernel='poly',degree=degree).fit(X_train, y_train) # SVM 開練
        y_pred = classifier.predict(X_test) # 預測
        print(confusion_matrix(y_test, y_pred)) # 繪畫出 confusion matrix
        print(classification_report(y_test, y_pred)) # 印出 accuracy、 macro avg、weighted avg
        print("==========================================")

    print("DEGREE = gaussian")# 印出 gaussian
    classifier = SVC(kernel='rbf').fit(X_train, y_train)# SVM 開練
    y_pred = classifier.predict(X_test)# 預測
    print(confusion_matrix(y_test, y_pred))# 繪畫出 confusion matrix
    print(classification_report(y_test, y_pred))# 印出 accuracy、 macro avg、weighted avg
    print("==========================================")

